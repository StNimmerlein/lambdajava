# How to use

Run `./gradlew buildFullZip` and upload the created zip file in `build/distributions` as Lambda with handler `de.andrena.aws.demo.Greeter::handleRequest` to AWS.