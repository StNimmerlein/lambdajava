package de.andrena.aws.demo;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Greeter implements RequestHandler<Person, String> {
    @Override
    public String handleRequest(Person person, Context context) {
        if (person.getAge() < 18) {
            return "Hi " + person.getFirstName();
        }
        return "Hello Mr/Mrs " + person.getLastName();
    }
}
